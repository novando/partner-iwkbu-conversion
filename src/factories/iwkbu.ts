import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/iwkbu`

export default {
  getAll (query?:IwkbuSearchType):Promise<responseType> {
    return libFetch.getData(`${apiUrl}`, query)
  },
  downloadAll (query?:IwkbuSearchType):Promise<void> {
    return libFetch.dlData(`${apiUrl}/download-all`, 'koversiIwkbu.csv', query)
  },
  create (payload:IwkbuYearCreateDataType):Promise<responseType> {
    return libFetch.postData(`${apiUrl}`, payload)
  },
  update (id:number, payload:BaseIwkbuDataType):Promise<responseType> {
    return libFetch.putData(`${apiUrl}/${id}`, payload)
  },
}