import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/official`

export default {
  async get5 (query?:OfficeQueryType):Promise<responseType> {
    return await libFetch.getData(`${apiUrl}`, query)
  },
  async getHq5 (query?:OfficeQueryType):Promise<responseType> {
    return libFetch.getData(`${apiUrl}/hq`, query)
  }
}