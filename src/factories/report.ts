import libFetch from '@arutek/core-app/libraries/fetch'
import libDate from '@arutek/core-app/libraries/date'

const apiUrl = `${import.meta.env.VITE_API_URL}/report`

export default {
  async conversionSummary (query:ConversionReportRequestDataType):Promise<responseType> {
    return await libFetch.postData(`${apiUrl}/conversion-summary`, query)
  },
  async officialSummary (query:ConversionReportRequestDataType):Promise<responseType> {
    return await libFetch.postData(`${apiUrl}/official-summary`, query)
  },
  async dlOfficialSummary (query:ConversionReportRequestDataType):Promise<void> {
    return await libFetch.dlPostData(`${apiUrl}/official-summary/download`, `OfficialReport-${libDate.jsDateToHtmlDate(new Date())}.pdf`, query)
  },
  async dlConversionDetail (query:ConversionReportRequestDataType):Promise<void> {
    return await libFetch.dlPostData(`${apiUrl}/conversion-summary/download`, `ConversionDetail-${libDate.jsDateToHtmlDate(new Date())}.pdf`, query)
  },
}