import { RouteObject } from 'react-router-dom'
import Dashboard from '@pages/Dashboard'
import Home from '@pages/Home'
// import Maintenance from '@pages/Maintenance'


const routers:RouteObject[] = [
  // { // Used only for maintenance
  //   path: '/:something?',
  //   element: <Maintenance />
  // },
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/dashboard',
    element: <Dashboard />,
  },
]

export default routers