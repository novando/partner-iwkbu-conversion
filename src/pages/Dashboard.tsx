import { useState, useEffect, ChangeEvent } from 'react'
import { useNavigate } from 'react-router-dom'
import Header from '@src/components/Header'
import facReport from '@factories/report'
import notification from '@arutek/core-app/helpers/notification'
import helpFormatter from '@arutek/core-app/helpers/formatter'
import imgJr from '@arutek/core-app/images/jasaraharja/logo.png'
import facOfficial from '@factories/official'
import libDate from '@arutek/core-app/libraries/date'

const Dashboard = () => {
  const navigate = useNavigate()
  const [isLoading, setIsLoading] = useState(false)
  const [isDownloading, setIsDownloading] = useState(false)
  const [showOfficials, setShowOfficials] = useState(false)
  const [showHqs, setShowHqs] = useState(false)
  const [officialSearch, setOfficialSearch] = useState('')
  const [hqSearch, setHqSearch] = useState('')
  const [reportMode, setReportMode] = useState('conversion')
  const [hqOptions, setHqOptions] = useState<HqDataType[]>([])
  const [periodSince, setPeriodSince] = useState(libDate.jsDateToHtmlDate(new Date((new Date))))
  const [periodTill, setPeriodTill] = useState(libDate.jsDateToHtmlDate(libDate.jsDateAddDays(1)))
  const [officialOptions, setOfficialOptions] = useState<OfficialDataType[]>([])
  const [filter, setFilter] = useState<ConversionReportRequestDataType>({since: libDate.jsDateToIso(new Date((new Date).setHours(0,0,0,0))), till: libDate.jsDateToIso(new Date((new Date).setHours(23,59,59,0)))})
  const [conversionDetail, setConversionDetail] = useState<ReportConvDetailDataType[]>([])
  const [officialDetail, setOfficialDetail] = useState<ReportOfficialDetailDataType[]>([])
  const [conversionSummary, setConversionSummary] = useState<ReportConvSummaryDataType>({
    lastIncome: 0,
    currIncome: 0,
    lastNopol: 0,
    currNopol: 0,
  })
  let letOfficialSearch:string
  let letHqSearch:string

  useEffect(() => {
    init()
  }, [])

  const init = () => {
    search()
  }
  const changedPeriodSince = (e:ChangeEvent<HTMLInputElement>) => {
    setPeriodSince(e.target.value)
  }
  const changedPeriodTill = (e:ChangeEvent<HTMLInputElement>) => {
    setPeriodTill(e.target.value)
  }
  const changedOffice = (e:ChangeEvent<HTMLInputElement>) => {
    setOfficialSearch(e.target.value)
    letOfficialSearch = e.target.value
    if (e.target.value === '') {
      setFilter(Object.assign(filter, {office:''}))
      return setShowOfficials(false)
    }
    searchOffice()
  }
  const officeSelected = (officialData:OfficialDataType) => {
    setOfficialSearch(officialData.name)
    setFilter(Object.assign(filter, {office: officialData.name}))
    setShowOfficials(false)
  }
  const searchOffice = () => {
    facOfficial.get5({search: letOfficialSearch, hq: hqSearch})
      .then((res) => {
        setOfficialOptions(res.data)
        if (res.data.length > 0) {
          setShowOfficials(true)
        } else {
          setShowOfficials(false)
        }
      })
      .catch((err) => {
        notification.notifyError(err.message.toString())
      })
  }
  const changedHq = (e:ChangeEvent<HTMLInputElement>) => {
    setHqSearch(e.target.value)
    letHqSearch = e.target.value
    if (e.target.value === '') {
      setFilter(Object.assign(filter, {hq:''}))
      return setShowHqs(false)
    }
    searchHq()
  }
  const hqSelected = (HqData:HqDataType) => {
    setHqSearch(HqData.code)
    setFilter(Object.assign(filter, {hq: HqData.code}))
    setShowHqs(false)
  }
  const searchHq = () => {
    facOfficial.getHq5({search: letHqSearch})
      .then((res) => {
        setHqOptions(res.data)
        if (res.count && res.count > 0) setShowHqs(true)
      })
      .catch((err) => {
        notification.notifyError(err.message.toString())
      })
  }
  const plusMinClass = (first:number, second:number) => {
    const val = first - second
    const colorClass = val < 0 ? 'text-danger' : 'text-success' 
    return `${colorClass} text-center text-4xl font-medium`
  }
  const calculateQty = (val:ReportConvDetailDataType[], pos:'top'|'bot') => {
    let acc = 0
    if (pos === 'bot') {
      val.forEach((item) => {
        if (['TANPA CATATAN', 'PELUNASAN'].includes(item.name.toUpperCase())) {
          acc = acc 
        } else {
          acc += item.qty
        }
      })
    } else {
      val.forEach((item) => {
        if (!['TANPA CATATAN', 'PELUNASAN'].includes(item.name.toUpperCase())) {
          acc = acc 
        } else {
          acc += item.qty
        }
      })
    }
    return acc
  }
  const calculatePrice = (val:ReportConvDetailDataType[], pos:'top'|'bot') => {
    let acc = 0
    if (pos === 'bot') {
      val.forEach((item) => {
        if (['TANPA CATATAN', 'PELUNASAN'].includes(item.name.toUpperCase())) {
          acc = acc 
        } else {
          acc += item.price
        }
      })
    } else {
      val.forEach((item) => {
        if (!['TANPA CATATAN', 'PELUNASAN'].includes(item.name.toUpperCase())) {
          acc = acc 
        } else {
          acc += item.price
        }
      })
    }
    return acc
  }
  const detailTransactionClass = (score:number, appendClass:string) => {
    if (score > 0) {
      return `${appendClass} text-navy-50`
    } else if (score < 0) {
      return `${appendClass} text-danger`
    } else {
      return `${appendClass} text-success`
    }
  }
  const loadingState = (isloading:boolean, count:number) => {
    if (isloading) {
      return (<p className="py-24">Loading...</p>)
    } else if (count > 0) {
      return (<p className="py-24">Tersedia { helpFormatter.numbering(count) } data</p>)
    } else {
      return (<p className="py-24">Data tidak tersedia</p>)
    }
  }
  const search = () => {
    setIsLoading(true)
    const newFilter = Object.assign({}, filter)
    if (periodSince.length === 10) newFilter.since = libDate.htmlDateToIso(periodSince)
    if (periodTill.length === 10) newFilter.till = libDate.htmlDateToIso(periodTill)
    setFilter(newFilter)
    facReport.conversionSummary(newFilter)
      .then((res) => {
        setConversionDetail(res.data.details)
        setConversionSummary(res.data.summary)
        return facReport.officialSummary(newFilter)
      })
      .then((res) => {
        setOfficialDetail(res.data)
      })
      .catch((err:any) => {
        notification.notifyError(err.message.toString())
      })
      .finally(() => {
        setIsLoading(false)
      })
  }
  const switcherClass = (currentReport = 'conversion') => {
    const addClass = reportMode === currentReport ? 'border-primary' : 'border-white'
    return `${addClass.trim()} group-hover:border-light-primary border-b-8 text-center`
  }
  const downloadOfficialAsPdf = () => {
    setIsDownloading(true)
    facReport.dlOfficialSummary(filter)
      .then(() => {
        notification.notifySuccess("Berkas berhasil diunduh")
      })
      .catch((err:any) => {
        notification.notifyError(err.message.toString())
      })
      .finally(() => {
        setIsDownloading(false)
      })
  }
  const downloadConversionAsPdf = () => {
    setIsDownloading(true)
    facReport.dlConversionDetail(filter)
      .then(() => {
        notification.notifySuccess("Berkas berhasil diunduh")
      })
      .catch((err:any) => {
        notification.notifyError(err.message.toString())
      })
      .finally(() => {
        setIsDownloading(false)
      })
  }
  const conversionDetailFiltered = (pos:'top'|'bot') => {
    if (pos === 'top') return conversionDetail.filter((item) => ['TANPA CATATAN', 'PELUNASAN'].includes(item.name))
    return conversionDetail.filter((item) => !['TANPA CATATAN', 'PELUNASAN'].includes(item.name))
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20">
          <img className="mx-auto w-160 my-20" src={imgJr} alt="Jasaraharja" />
          <h1 className="typ-title-regular-bold mb-8">Check In - Check Out</h1>
          <p className="typ-title-small-medium">Aplikasi Konversi Penerimaan IWKBU</p>
        </section>
        <section className="bg-white rounded-lg shadow p-20 mb-20">
          <section className="flex items-center gap-20 mb-24">
            <div className="flex flex-col gap-16">
              <div className="relative">
                <label className="flex items-center gap-8">
                  <p className="min-w-120">Kantor</p>
                  <input 
                    type="text"
                    className="input"
                    value={hqSearch}
                    placeholder='Cabang'
                    onChange={changedHq} />
                </label>
                {showHqs && (
                  <div className="absolute z-50 rounded w-full bg-white border drop-shadow">
                    {hqOptions.map((item, key) => (
                      <p key={key} onClick={() => hqSelected(item)} className="hover:bg-dark-grey-30 cursor-pointer p-4">{item.code}</p>
                    ))}
                  </div>
                )}
              </div>
              <div className="relative">
                <label className="flex items-center gap-8">
                  <p className="min-w-120">Loket</p>
                  <input 
                    type="text"
                    className="input"
                    value={officialSearch}
                    placeholder='Perwakilan'
                    onChange={changedOffice} />
                </label>
                {showOfficials && (
                  <div className="absolute z-50 rounded w-full bg-white border drop-shadow">
                    {officialOptions.map((item, key) => (
                      <p key={key} onClick={() => officeSelected(item)} className="hover:bg-dark-grey-30 cursor-pointer p-4">{item.name}</p>
                    ))}
                  </div>
                )}
              </div>
            </div>
            <div className="flex flex-col gap-16">
              <div className="flex items-center gap-8">
                <p className="min-w-120">Awal transaksi</p>
                <input 
                  type="date"
                  className="input"
                  value={periodSince}
                  onChange={changedPeriodSince} />
              </div>
              <div className="flex items-center gap-8">
                <p className="min-w-120">Akhir transaksi</p>
                <input 
                  type="date"
                  className="input"
                  value={periodTill}
                  onChange={changedPeriodTill} />
              </div>
            </div>
            <button className="btn btn-primary" onClick={search}>Cari</button>
          </section>
          <section className="flex justify-evenly mb-24">
            <div className="grid grid-cols-2 gap-32">
              <div>
                <p className="text-center">Jumlah Nopol Tahun Ini</p>
                <p className="text-center text-4xl font-medium">{ helpFormatter.numbering(conversionSummary.currNopol) }</p>
              </div>
              <div>
                <p className="text-center">Jumlah Nopol Tahun Lalu</p>
                <p className="text-center text-4xl font-medium">{ helpFormatter.numbering(conversionSummary.lastNopol) }</p>
              </div>
              <div className="col-span-2">
                <p className="text-center">Selisih Jumlah Nopol</p>
                <p className={plusMinClass(conversionSummary.currNopol, conversionSummary.lastNopol)}>{ helpFormatter.numbering(conversionSummary.currNopol - conversionSummary.lastNopol) }</p>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-32">
              <div>
                <p className="text-center">Penerimaan Tahun Ini</p>
                <p className="text-center text-4xl font-medium">{ helpFormatter.shortCurrency(conversionSummary.currIncome) }</p>
              </div>
              <div>
                <p className="text-center">Penerimaan Tahun Lalu</p>
                <p className="text-center text-4xl font-medium">{ helpFormatter.shortCurrency(conversionSummary.lastIncome) }</p>
              </div>
              <div className="col-span-2">
                <p className="text-center">Selisih Penerimaan</p>
                <p className={plusMinClass(conversionSummary.currIncome, conversionSummary.lastIncome)}>{ helpFormatter.shortCurrency(conversionSummary.currIncome - conversionSummary.lastIncome) }</p>
              </div>
            </div>
          </section>
          <section className="mx-60 flex justify-between border border-b-0 border-dark-grey-30 rounded-t-lg">
            <div onClick={() => setReportMode('conversion')} className={(reportMode === 'conversion' ? 'border-r border-dark-grey-30 bg-dark-grey-10' : '') + " group w-full cursor-pointer rounded-t-lg px-16 pb-8 pt-16"}>
              <p className={switcherClass('conversion')}>Monitoring Konversi</p>
            </div>
            <div onClick={() => setReportMode('official')} className={(reportMode === 'official' ? 'border-l border-dark-grey-30 bg-dark-grey-10' : '') + " group w-full cursor-pointer rounded-t-lg px-16 pb-8 pt-16"}>
              <p className={switcherClass('official')}>Konversi per Loket</p>
            </div>
          </section>
          <section className="px-60 pb-32">
            {reportMode === 'conversion' ? (
              <>
                <table className="rounded-lg w-full mb-16">
                  <thead>
                    <tr className="bg-light-primary">
                      <th className="text-white py-8">Keterangan</th>
                      <th className="text-white py-8">Jumlah Nopol</th>
                      <th className="text-white py-8">Nominal</th>
                    </tr>
                  </thead>
                  <tbody>
                    {conversionDetailFiltered('top').map((item, key) => (
                      <tr key={key} className={key % 2 === 0 ? 'bg-dark-grey-20' : 'bg-white'}>
                        <td className={detailTransactionClass(item.score, 'px-16 py-4')}>{ item.name }</td>
                        <td className={detailTransactionClass(item.score, 'text-center px-16 py-4')}>{ helpFormatter.numbering(item.qty) }</td>
                        <td className={detailTransactionClass(item.score, 'text-end px-16 py-4')}>{ helpFormatter.currency(item.price) }</td>
                      </tr>
                    ))}
                    <tr className="bg-primary text-white">
                      <td className="text-center px-16 py-4">TOTAL</td>
                      <td className="text-center px-16 py-4">{ helpFormatter.numbering(calculateQty(conversionDetail, 'top')) }</td>
                      <td className="text-end px-16 py-4">{ helpFormatter.currency(calculatePrice(conversionDetail, 'top')) }</td>
                    </tr>
                  </tbody>
                </table>
                <table className="rounded-lg w-full mb-32">
                  <thead>
                    <tr className="bg-light-primary">
                      <th className="text-white py-8">Konversi IWKBU</th>
                      <th className="text-white py-8">Jumlah Nopol</th>
                      <th className="text-white py-8">Nominal</th>
                    </tr>
                  </thead>
                  <tbody>
                    {conversionDetailFiltered('bot').map((item, key) => (
                      <tr key={key} className={key % 2 === 0 ? 'bg-dark-grey-20' : 'bg-white'}>
                        <td className={detailTransactionClass(item.score, 'px-16 py-4')}>{ item.name }</td>
                        <td className={detailTransactionClass(item.score, 'text-center px-16 py-4')}>{ helpFormatter.numbering(item.qty) }</td>
                        <td className={detailTransactionClass(item.score, 'text-end px-16 py-4')}>{ helpFormatter.currency(item.price) }</td>
                      </tr>
                    ))}
                    <tr className="bg-primary text-white">
                      <td className="text-center px-16 py-4">TOTAL KONVERSI IWKBU</td>
                      <td className="text-center px-16 py-4">{ helpFormatter.numbering(calculateQty(conversionDetail, 'bot')) }</td>
                      <td className="text-end px-16 py-4">{ helpFormatter.currency(calculatePrice(conversionDetail, 'bot')) }</td>
                    </tr>
                  </tbody>
                </table>
                <button onClick={downloadConversionAsPdf} className="btn btn-primary" disabled={isDownloading}>Download PDF</button>
              </>
            ) : (
              <>
                <table className="rounded-lg w-full mb-32">
                  <thead>
                    <tr className="bg-light-primary">
                      <th className="text-white py-8">Loket</th>
                      <th className="text-white py-8">Samsat</th>
                      <th className="text-white py-8">Jumlah Nopol Tahun Lalu</th>
                      <th className="text-white py-8">Nominal IWKBU Tahun Lalu</th>
                      <th className="text-white py-8">Jumlah Nopol Tahun Ini</th>
                      <th className="text-white py-8">Nominal IWKBU Tahun Ini</th>
                      <th className="text-white py-8">Outstanding Nopol</th>
                      <th className="text-white py-8">Outstanding Penerimaan</th>
                    </tr>
                  </thead>
                  <tbody>
                    {officialDetail.map((item, key) => (
                      <tr key={key} className={key % 2 === 0 ? 'bg-dark-grey-20' : 'bg-white'}>
                        <td className={'text-center px-16 py-4'}>{ item.hq }</td>
                        <td className={'text-center px-16 py-4'}>{ item.office }</td>
                        <td className={'text-center px-16 py-4'}>{ helpFormatter.numbering(item.lastQty) }</td>
                        <td className={'text-end px-16 py-4'}>{ helpFormatter.currency(item.lastIncome) }</td>
                        <td className={'text-center px-16 py-4'}>{ helpFormatter.numbering(item.currQty) }</td>
                        <td className={'text-end px-16 py-4'}>{ helpFormatter.currency(item.currIncome) }</td>
                        <td className={'text-center px-16 py-4'}>{ helpFormatter.numbering(item.sumQty) }</td>
                        <td className={'text-end px-16 py-4'}>{ helpFormatter.currency(item.sumIncome) }</td>
                      </tr>
                    ))}
                    <tr className="bg-primary text-white">
                      <td className="text-center px-16 py-4" colSpan={2}>Grand total</td>
                      <td className="text-center px-16 py-4">{ helpFormatter.numbering(officialDetail.reduce((acc, item) => acc + item.lastQty, 0)) }</td>
                      <td className="text-end px-16 py-4">{ helpFormatter.currency(officialDetail.reduce((acc, item) => acc + item.lastIncome, 0)) }</td>
                      <td className="text-center px-16 py-4">{ helpFormatter.numbering(officialDetail.reduce((acc, item) => acc + item.currQty, 0)) }</td>
                      <td className="text-end px-16 py-4">{ helpFormatter.currency(officialDetail.reduce((acc, item) => acc + item.currIncome, 0)) }</td>
                      <td className="text-center px-16 py-4">{ helpFormatter.numbering(officialDetail.reduce((acc, item) => acc + item.sumQty, 0)) }</td>
                      <td className="text-end px-16 py-4">{ helpFormatter.currency(officialDetail.reduce((acc, item) => acc + item.sumIncome, 0)) }</td>
                    </tr>
                  </tbody>
                </table>
                <button onClick={downloadOfficialAsPdf} className="btn btn-primary" disabled={isDownloading}>Download PDF</button>
              </>
            )}
          </section>
        </section>
      </main>
    </>
  )
}

export default Dashboard