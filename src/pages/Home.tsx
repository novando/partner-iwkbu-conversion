import { useState, useEffect, useRef, ChangeEvent } from 'react'
import { debounce } from 'lodash'
import Header from '@src/components/Header'
import facIwkbu from '@factories/iwkbu'
import facOfficial from '@factories/official'
import facReason from '@factories/reason'
import notification from '@arutek/core-app/helpers/notification'
import libDate from '@arutek/core-app/libraries/date'
import helpFormatter from '@arutek/core-app/helpers/formatter'
import imgJr from '@arutek/core-app/images/jasaraharja/logo.png'

const defaultNewVehicle = {
  nopol: '',
  price: 0,
  iwkbu: libDate.jsDateToIso(new Date((new Date).setHours(0,0,0,0))),
  swdkllj: libDate.jsDateToIso(new Date((new Date).setHours(0,0,0,0))),
  recorded: libDate.jsDateToIso(new Date((new Date).setHours(0,0,0,0))),
  inputedOn: libDate.jsDateToIso(new Date((new Date).setHours(0,0,0,0))),
  kantor: '',
  note: '',
  reasonId: 1,
}

const loadingState = (isloading:boolean, countBefore:number, countCurr:number, priceBefore:number, priceCurrent:number) => {
  if (isloading) {
    return (<p className="text-center">Loading...</p>)
  } else if ((countBefore + countCurr) > 0) {
    return (
      <>
        <h3 className="text-center typ-title-small-medium">Summary</h3>
        <section className="flex justify-evenly mx-16">
          <div>
            <p>Data Tahun Lalu</p>
            <p>Total Nopol : <span className="font-bold">{ helpFormatter.numbering(countBefore) }</span> Kendaraan</p>
            <p>Penerimaan : <span className="font-bold">{ helpFormatter.currencyNoDecimal(priceBefore) }</span></p>
          </div>
          <div>
            <p>Data Tahun Ini</p>
            <p>Total Nopol : <span className="font-bold">{ helpFormatter.numbering(countCurr) }</span> Kendaraan</p>
            <p>Penerimaan : <span className="font-bold">{ helpFormatter.currencyNoDecimal(priceCurrent) }</span></p>
          </div>
        </section>
      </>)
  } else {
    return (<p>Data tidak tersedia</p>)
  }
}

const Home = () => {
  const testRef = useRef(null)
  const [isLoading, setIsLoading] = useState(true)
  const [isDownloading, setIsDownloading] = useState(false)
  const [saving, setSaving] = useState(-1)
  const [showOfficials, setShowOfficials] = useState(false)
  const [iwkbus, setIwkbus] = useState<IwkbuDataType[]>([])
  const [bakIwkbu, setBakIwkbu] = useState<IwkbuDataType>()
  const [reasonOptions, setReasonOptions] = useState<ReasonDataType[]>([])
  const [filter, setFilter] = useState<IwkbuSearchType>({})
  const [newVehicle, setNewVehicle] = useState<IwkbuYearCreateDataType>(defaultNewVehicle)
  const [countBefore, setCountBefore] = useState(0)
  const [countCurr, setCountCurr] = useState(0)
  const [currEdit, setCurrEdit] = useState(-1)
  const [officialSearch, setOfficialSearch] = useState('')
  const [officialOptions, setOfficialOptions] = useState<OfficialDataType[]>([])
  const [period, setPeriod] = useState('')
  let letOfficialSearch:string
  
  useEffect(() => {
    init()
  }, [])

  const init = () => {
    facOfficial.get5()
      .then((res) => {
        const now = libDate.jsDateToHtmlMonth(new Date())
        setOfficialSearch(res.data[0].name)
        defaultNewVehicle.kantor = res.data[0].name
        setPeriod(now)
        setNewVehicle({...newVehicle, kantor: res.data[0].name})
        return search({official: res.data[0].name, period: libDate.htmlMonthToIso(now)})
      })
      .then(() => {
        return facReason.getAll() 
      })
      .then((res) => {
        setReasonOptions(res.data)
      })
      .catch((err:any) => {
        notification.notifyError(err.message.toString())
      })
  }
  const changedOffice = (e:ChangeEvent<HTMLInputElement>) => {
    setOfficialSearch(e.target.value)
    letOfficialSearch = e.target.value
    defaultNewVehicle.kantor = e.target.value
    if (e.target.value === '') {
      return setShowOfficials(false)
    }
    if (e.target.value.includes(' - ')) return
    debounceOfficeSearch()
  }
  const officeSelected = (officialData:OfficialDataType) => {
    setOfficialSearch(officialData.name)
    setNewVehicle({...newVehicle, kantor: officialData.name})
    search({official: officialData.name})
    setShowOfficials(false)
  }
  const changedPeriod = (e:ChangeEvent<HTMLInputElement>) => {
    const period = libDate.htmlMonthToIso(e.target.value)
    setPeriod(e.target.value)
    search({period})
  }
  const search = (val:IwkbuSearchType) => {
    setIsLoading(true)
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    setCountBefore(0)
    setCountCurr(0)
    let nopolCurr = 0
    let nopolBefore = 0
    facIwkbu.getAll(newFilter)
      .then((res) => {
        const newData:IwkbuDataType[] = []
        res.data.forEach((item:IwkbuDataType) => {
          const currRecorded = libDate.isoToHtmlDate(item.currRecorded)
          const lastRecorded = libDate.isoToHtmlDate(item.lastRecorded)
          if (libDate.htmlDateToDate1(currRecorded) !== '-') nopolCurr++
          if (libDate.htmlDateToDate1(lastRecorded) !== '-') nopolBefore++
          item.currIwkbu = libDate.isoToHtmlDate(item.currIwkbu)
          item.currRecorded = currRecorded
          item.currSwdkllj = libDate.isoToHtmlDate(item.currSwdkllj)
          newData.push(item)
        })
        setIwkbus(newData)
      })
      .catch((err:any) => {
        notification.notifyError(err.message.toString())
      })
      .finally(() => {
        setCountBefore(nopolBefore)
        setCountCurr(nopolCurr)
        setIsLoading(false)
      })
  }
  const searchOffice = () => {
    facOfficial.get5({search: letOfficialSearch})
      .then((res) => {
        setOfficialOptions(res.data)
        if (res.data.length && res.data.length > 0) setShowOfficials(true)
      })
      .catch((err) => {
        notification.notifyError(err.message.toString())
      })
  }
  const changedCurrReasonId = (e:ChangeEvent<HTMLSelectElement>, key:number) => {
    const newVal = [...iwkbus]
    newVal[key]['reasonId'] = parseInt(e.target.value)
    setIwkbus(newVal)
  }
  const changedCurrNote = (e:ChangeEvent<HTMLInputElement>, key:number) => {
    const newVal = [...iwkbus]
    newVal[key]['note'] = e.target.value
    setIwkbus(newVal)
  }
  const saveData = (key:number) => {
    setSaving(key)
    const payload:BaseIwkbuDataType = {
      note: iwkbus[key].note,
      reasonId: iwkbus[key].reasonId,
    }
    facIwkbu.update(iwkbus[key].id, payload)
      .then((res) => {
        notification.notifySuccess(res.message)
        setCurrEdit(-1)
      })
      .catch((err) => {
        notification.notifyError(err.message)
      })
      .finally(() => {
        setSaving(-1)
      })
  }
  const currInput = () => {
    if (testRef.current) {
      (testRef.current as any).select()
    }
  }
  const editNopol = (key:number) => {
    if (key === currEdit) return
    cancelEdit(currEdit)
    setBakIwkbu({...iwkbus[key]})
    setCurrEdit(key)
  }
  const cancelEdit = (key:number) => {
    if (bakIwkbu) {
      const newVal = [...iwkbus]
      newVal[key] = bakIwkbu
      setIwkbus(newVal)
    }
    setCurrEdit(-1)
  }
  const downloadAll = () => {
    setIsDownloading(true)
    facIwkbu.downloadAll(filter)
      .catch((err) => {
        notification.notifyError(err.message)
      })
      .finally(() => {
        setIsDownloading(false)
      })
  }
  const debounceOfficeSearch = debounce(searchOffice, 333)
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20">
          <img className="mx-auto w-160 my-20" src={imgJr} alt="Jasaraharja" />
          <h1 className="typ-title-regular-bold mb-8">Check In - Check Out</h1>
          <p className="typ-title-small-medium">Aplikasi Konversi Penerimaan IWKBU</p>
        </section>
        <section className="bg-white rounded-lg shadow flex flex-wrap gap-20 p-20 mb-20">
          <div className="flex justify-between w-full">
            <div className="flex items-center gap-20">
              <div className="relative">
                <label className="flex items-center gap-8">
                  <p>Loket</p>
                  <input 
                    type="text"
                    className="input"
                    value={officialSearch}
                    ref={testRef}
                    placeholder='Perwakilan'
                    onChange={changedOffice}
                    onClick={currInput} />
                </label>
                {showOfficials && (
                  <div className="absolute z-50 rounded w-full bg-white border drop-shadow">
                    {officialOptions.map((item, key) => (
                      <p key={key} onClick={() => officeSelected(item)} className="hover:bg-dark-grey-30 cursor-pointer p-4">{item.name}</p>
                    ))}
                  </div>
                )}
              </div>
              <div className="flex items-center gap-8">
                <p>Periode transaksi</p>
                <input 
                  type="month"
                  className="input"
                  value={period}
                  placeholder='Periode (YYYY-MM)'
                  onChange={changedPeriod} />
              </div>
            </div>
            <button onClick={() => downloadAll()} className="btn btn-primary disabled:bg-light-primary" type="button" disabled={isDownloading}>Download CSV Peride Ini</button>
          </div>
          <section className="w-full">
            {loadingState(
              isLoading,
              countBefore,
              countCurr,
              iwkbus.reduce((total, item) => { return item.lastPrice + total }, 0),
              iwkbus.reduce((total, item) => { return item.currPrice + total }, 0),
            )}
          </section>
          <section className="overflow-auto max-h-480 cursor-default rounded">
            <table className="w-max">
              <thead className="sticky z-20 top-0">
                <tr>
                  <th rowSpan={2} className="sticky z-10 left-0 px-16 py-8 bg-primary text-contrast-primary">Nopol</th>
                  <th colSpan={4} className="px-16 py-8 bg-light-primary text-contrast-primary">Masa Laku Terakhir Pada Tahun Sebelum</th>
                  <th colSpan={4} className="px-16 py-8 bg-light-primary text-contrast-primary">Masa Laku Terakhir Pada Tahun Ini</th>
                  <th rowSpan={2} className="px-16 py-8 bg-light-primary text-contrast-primary">Outstanding Penerimaan</th>
                  <th rowSpan={2} className="px-16 py-8 bg-light-primary text-contrast-primary">Konversi IWKBU</th>
                  <th rowSpan={2} className="px-16 py-8 bg-light-primary text-contrast-primary">Keterangan</th>
                  <th rowSpan={2} className="px-16 py-8 bg-light-primary text-contrast-primary" />
                </tr>
                <tr>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">Tanggal Transaksi</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">SWDKLLJ</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">IWKBU</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">Penerimaan IWKBU</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">Tanggal Transaksi</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">SWDKLLJ</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">IWKBU</th>
                  <th className="px-16 py-8 bg-light-primary text-contrast-primary">Penerimaan IWKBU</th>
                </tr>
              </thead>
              <tbody>
                {iwkbus.map((item, key) => (
                  <tr key={key} className="hover:bg-dark-grey-40">
                    <td className="sticky z-10 left-0 px-16 py-4 bg-dark-grey-10">{ item.nopol }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">{ libDate.isoToDate1(item.lastRecorded) }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">{ libDate.isoToDate1(item.lastSwdkllj) }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">{ libDate.isoToDate1(item.lastIwkbu) }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">{ helpFormatter.currency(item.lastPrice) }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 cursor-pointer border-gray-300">{ libDate.htmlDateToDate1(item.currRecorded, 'idID') }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 cursor-pointer border-gray-300">{ libDate.htmlDateToDate1(item.currSwdkllj, 'idID') }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 cursor-pointer border-gray-300">{ libDate.htmlDateToDate1(item.currIwkbu, 'idID') }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 cursor-pointer border-gray-300">{ helpFormatter.currency(item.currPrice) }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">{ helpFormatter.currency(item.diffPrice) }</td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">
                      <select className="input" value={item.reasonId} onChange={(e) => changedCurrReasonId(e, key)}>
                        {reasonOptions.map((item, key) => (
                          <option key={key} value={item.id}>{ item.name }</option>
                        ))}
                      </select>
                    </td>
                    <td onClick={() => editNopol(key)} className="px-16 py-4 border-gray-300">
                      <input type="text" className="input" value={ item.note } onChange={(e) => changedCurrNote(e, key)}/>
                    </td>
                    {currEdit === key && (
                      <td className="sticky flex gap-16 z-10 right-0 px-16 py-4 bg-dark-grey-10">
                        <button onClick={() => saveData(key)} disabled={saving === key} className="btn btn-success">Simpan</button>
                        <button onClick={() => cancelEdit(key)} className="btn btn-danger">Batal</button>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          </section>
        </section>
      </main>
    </>
  )
}

export default Home
