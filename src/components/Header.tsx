
import MenuHeader from '@arutek/core-app/components/MenuHeader'
import routerList from '@src/libraries/router-list'

const Header = () => {
  return (
    <header className="mb-20">
      <MenuHeader routerList={routerList} />
    </header>
  )
}

export default Header